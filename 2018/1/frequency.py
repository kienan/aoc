#!/usr/bin/env python3

import sys

def delta_frequency(f = sys.stdin):
    df = 0
    frequency_count = {0: 1}
    frequency_list = []
    for l in f.readlines():
        frequency_list.append(l)
    first = None
    iteration = 1
    df_first_iteration = None
    while first is None:
        for l in frequency_list:
            df += int(l)
            if df not in frequency_count:
                frequency_count[df] = 1
            else:
                frequency_count[df] = frequency_count[df] + 1
            if not first:
                if frequency_count[df] >= 2:
                    first = df
        if df_first_iteration is None:
            df_first_iteration = df
    print(df_first_iteration)
    if first is not None:
        print("First duplicate frequency: {}".format(first))

if __name__ == '__main__':
    delta_frequency()
