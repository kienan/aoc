#!/usr/bin/env python3

import sys
def checksum(l):
    double = 0
    triple = 0
    for item in l:
        d, t = count_duplicate(item)
        double += d
        triple += t
    return double * triple

def count_duplicate(item):
    double = 0
    triple = 0
    c = dict()
    for i in item:
        if i in c:
            c[i] += 1
        else:
            c[i] = 1
    for k,v in c.items():
        if v == 2:
            double = 1
        if v == 3:
            triple = 1
    #print(c)
    #print("{}: {}, {}".format(item, double, triple))
    return (double, triple)

def compare(item, l):
    for candidate in l:
        if candidate == item:
            continue
        diff = 0
        x = 0
        while x < len(item):
            if item[x] != candidate[x]:
                diff += 1
            x += 1
            if diff > 1:
                break
        #if diff > 1:
        #    print("{} does not match {}: {} different".format(item, candidate, diff))
        if diff == 1:
            return candidate

def common(x, y):
    i = 0
    common = ""
    while i < len(x):
        if x[i] == y[i]:
            common += x[i]
        i += 1
    return common

if __name__ == '__main__':
    l = sys.stdin.readlines()
    print(checksum(l))
    for item in l:
        z = compare(item, l)
        if z is not None:
            print("{} / {}".format(item, z))
            print(common(item, z))
            break
