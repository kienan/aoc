fn main() {
    let contents = std::fs::read_to_string("input")
        .expect("Failed to read file 'input'");
    let it = contents.split("\n");
    let mut sum: u64 = 0;
    let mut sum2: u64 = 0;
    for val in it {
        if val == "" {
            continue;
        }
        let mut x = val.parse::<u64>().unwrap();
        x /= 3;
        x -= 2;
        sum += x;
        sum2 += x;
        while x > 0 {
            x /= 3;
            if x < 2 {
                x = 0;
            }
            else {
                x -= 2;
            }
            sum2 += x;
        }
    }
    println!("[Part 1] The sum is {}", sum);
    println!("[Part 2] The sum is {}", sum2);
}
