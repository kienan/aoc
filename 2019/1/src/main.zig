const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);

    var it = std.mem.tokenize(contents, "\n");
    var sum: u64 = 0;
    var sum2: u64 = 0;
    while (it.next()) |v| {
        var val: u64 = try std.fmt.parseInt(u64, v, 10);
        val /= 3;
        val -= 2;
        sum += val;
        sum2 += val;
        while (val > 0) {
            val /= 3;
            if (val < 2) {
                val = 0;
            }
            else {
                val -= 2;
            }
            sum2 += val;
        }
    }
    std.log.info("[Part 1] Sum: {}", .{sum});
    std.log.info("[Part 2] Sum: {}", .{sum2});
}
