fn main() {
    let contents = std::fs::read_to_string("input")
        .expect("Failed to read file 'input'");
    let it = contents.split(",");
    let mut program = Vec::new();
    for val in it {
        if val.ends_with('\n') {
            let mut s = String::from(val);
            s.pop();
            program.push(s.parse::<i32>().unwrap());
        }
        else {
            program.push(val.parse::<i32>().unwrap());
        }
    }

    // Create a copy of the memory to refresh from
    let backup = program.clone();

    // Modify state for part 1
    program[1] = 12;
    program[2] = 2;
    simulate(&mut program);
    println!("[Part 1] The value at position 0 is: {}", program[0]);

    let mut noun = 0;
    while noun < 100 {
        let mut verb = 0;
        while verb < 100 {
            program = backup.clone();
            program[1] = noun;
            program[2] = verb;
            simulate(&mut program);
            //println!("Noun {}, verb {}: {}", noun, verb, program[0]);
            if program[0] == 19690720 {
                println!("[Part 2] noun {}, verb {}, sum {}", noun, verb, 100*noun+verb);
                break;
            }
            verb += 1;
        }
        noun += 1;
    }
}

fn simulate(v: &mut Vec<i32>) {
    let mut pos = 0;
    loop {
        let code = v[pos];
        if code == 1 {
            let i1 = v[pos+1] as usize;
            let i2 = v[pos+2] as usize;
            let idest = v[pos+3] as usize;
            v[idest] = v[i1] + v[i2];
            pos += 4;
            continue;
        }
        else if code == 2 {
            let i1 = v[pos+1] as usize;
            let i2 = v[pos+2] as usize;
            let idest = v[pos+3] as usize;
            v[idest] = v[i1] * v[i2];
            pos += 4;
            continue;
        }
        else if code == 99 {
            break;
        }
        else {
            panic!("Unknown opcode");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_op_1() {
        let mut program = vec![1, 0, 0, 0, 99];
        simulate(&mut program);
        assert_eq!(program[0], 2);
    }

    #[test]
    fn test_op_2() {
        let mut program = vec![2, 3, 0, 3, 99];
        simulate(&mut program);
        assert_eq!(program[3], 6);
    }

    #[test]
    fn test_case_3() {
        let mut program = vec![2,4,4,5,99,0];
        simulate(&mut program);
        assert_eq!(program[program.len() - 1], 9801);
    }

    #[test]
    fn test_case_4() {
        let mut program = vec![1,1,1,4,99,5,6,0,99];
        simulate(&mut program);
        assert_eq!(program[0], 30);
        assert_eq!(program[4], 2);
    }
}
