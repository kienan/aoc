use std::collections::HashSet;
use std::collections::HashMap;

fn main() {
    let mut contents = std::fs::read_to_string("input")
        .expect("Failed to read file 'input'");
    let mut it = contents.split("\n");
    let mut line_a: HashSet<Point> = HashSet::new();
    let mut line_b: HashSet<Point> = HashSet::new();
    match it.next() {
        Some(v) => get_line_points_from_str(&mut line_a, &v),
        None    => panic!(),
    }
    match it.next() {
        Some(v) => {
            let s = String::from(v);
            get_line_points_from_str(&mut line_b, &s);
        },
        None    => panic!(),
    }

    // Some possible approaches
    // my first thought was to do the following
    //   1) collect all points for line_a and line_b
    //   2) create a pool of intersecting points
    //   3) for all the intersecting points, which is
    //   closest to the origin in manhattan distance
    //
    // this is pretty easy, but seems like it would be
    // memory hungry and doesn't scale to large lines
    // bonus: rust HashSets (wrappers around HashMap)
    // a union method to facilitate this task.
    //
    // another thought I had was to find and use some
    // sort of math things to compare to pair of line
    // segments to see if they intersect, and if so - where.

    let mut closest_point: Point = Point {x: 0, y: 0};
    let mut closest_distance = i32::MAX;
    let mut intersects: HashSet<Point> = HashSet::new();
    for p in line_a.intersection(&line_b) {
        if p.x == 0 && p.y == 0 {
            continue;
        }
        intersects.insert(Point {x: p.x, y: p.y});
        let distance = p.x.abs() + p.y.abs();
        //println!("Intersection: {},{} distance {}",
        //         p.x, p.y, distance);
        if distance < closest_distance {
            closest_point = Point {x: p.x, y: p.y};
            closest_distance = distance;
        }
    }
    println!("[Part 1] Closest intersection {} away at {},{}",
             closest_distance, closest_point.x, closest_point.y);

    // We replay the get_line_poionts_from_str, but instead
    // when we reach one of the intersections we can add
    // Point, steps into a result set.
    let mut line_a_steps_to_intersect: HashMap<Point, u64> = HashMap::new();
    let mut line_b_steps_to_intersect: HashMap<Point, u64> = HashMap::new();
    contents = std::fs::read_to_string("input")
        .expect("Failed to read file 'input'");
    it = contents.split("\n");
    match it.next() {
        Some(v) => get_steps_to_intersections(&intersects, v,
        &mut line_a_steps_to_intersect),
        None    => panic!(),
    }
    match it.next() {
        Some(v) => get_steps_to_intersections(&intersects, v,
        &mut line_b_steps_to_intersect),
        None    => panic!(),
    }
    let mut step_sum = u64::MAX;
    let iit = intersects.iter();
    for p in iit {
        let new_sum = line_a_steps_to_intersect.get(&p).unwrap()
            + line_b_steps_to_intersect.get(&p).unwrap();
        if new_sum < step_sum {
            step_sum = new_sum;
            closest_point = Point {x: p.x, y: p.y};
        }
    }
    println!("[Part 2] Intersection {},{} is {} steps away",
             closest_point.x, closest_point.y, step_sum);
}

fn get_steps_to_intersections(points: &HashSet<Point>, source: &str, map: &mut HashMap<Point, u64>) {
    let mut x = 0;
    let mut y = 0;
    let mut total_steps = 0;
    let lit = source.split(",");
    for val in lit {
        let dir = val.chars().nth(0).unwrap();
        let mut dx = 0;
        let mut dy = 0;
        let v = val[1..].parse::<i32>().unwrap();
        match dir {
            'U' => dy = 1,
            'D' => dy = -1,
            'L' => dx = -1,
            'R' => dx = 1,
            _   => panic!(),
        }
        let mut needed_steps = 0;
        if dx != 0 {
            needed_steps = v;
            needed_steps = needed_steps.abs();
        }
        if dy != 0 {
            needed_steps = v;
            needed_steps = needed_steps.abs();
        }
        let mut step = 0;
        while step < needed_steps {
            x += dx;
            y += dy;
            total_steps += 1;
            let p = Point {x: x, y: y};
            if points.contains(&p) {
                //println!("Inserting intersection {},{} at {} steps",
                //p.x, p.y, total_steps);
                map.insert(p, total_steps);
            }
            step += 1;
        }
    }
}

fn get_line_points_from_str(points: &mut HashSet<Point>, source: &str) {
    let mut x = 0;
    let mut y = 0;
    points.insert(Point { x: x, y: y });
    let lit = source.split(",");
    for val in lit {
        let dir = val.chars().nth(0).unwrap();
        let mut dx = 0;
        let mut dy = 0;
        let v = val[1..].parse::<i32>().unwrap();
        match dir {
            'U' => dy = 1,
            'D' => dy = -1,
            'L' => dx = -1,
            'R' => dx = 1,
            _   => panic!(),
        }
        let mut needed_steps = 0;
        if dx != 0 {
            needed_steps = v;
            needed_steps = needed_steps.abs();
        }
        if dy != 0 {
            needed_steps = v;
            needed_steps = needed_steps.abs();
        }
        //println!("Walking from {},{} {}{} (step {},{} {} times)",
        //x, y, dir, v, dx, dy, needed_steps);
        let mut step = 0;
        while step < needed_steps {
            x += dx;
            y += dy;
            //println!("Adding {},{}", x, y);
            points.insert(Point {x: x, y: y});
            step += 1;
        }
    }
}

#[derive(PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}
#[cfg(test)]
mod tests {

}
