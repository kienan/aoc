use icc;

fn main() {
    let contents = std::fs::read_to_string("input")
        .expect("Failed to read file 'input'");
    let it = contents.split(",");
    let mut program = Vec::new();
    for val in it {
        if val.ends_with('\n') {
            let mut s = String::from(val);
            s.pop();
            program.push(s.parse::<i32>().unwrap());
        }
        else {
            program.push(val.parse::<i32>().unwrap());
        }
    }

    // Create a copy of the memory to refresh from
    let backup = program.clone();

    let mut output: i32 = 0;
    println!("Start part 1");
    icc::simulate(&mut program, 1, &mut output);

    program = backup.clone();
    println!("\nStart part 2");
    icc::simulate(&mut program, 5, &mut output);
}
