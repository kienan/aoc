use std::str::FromStr;

fn main() {
    let data = std::fs::read_to_string("input")
        .expect("Failed to read 'input'");
    let mut program: Vec<i64> = Vec::new();
    for v in data.split(",") {
        program.push(i64::from_str(v).unwrap());
    }

    {
        let mut c = icc::Computer::initialize(program.clone(), vec![1]);
        c.run();
        if c.output.len() > 1 {
            println!("Some op-codes report failures");
            println!("Test output: {:?}", c.output);
            return;
        }
        println!("[Part 1] {}", c.output[0]);
    }
    {
        let mut c = icc::Computer::initialize(program.clone(), vec![2]);
        c.run();
        println!("[Part 2] {}", c.output[0]);
    }
}
