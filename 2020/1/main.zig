const std = @import("std");

pub fn main() !void {
    var allocator = std.heap.GeneralPurposeAllocator(.{}){};
    var f = std.fs.File { .handle = try std.os.open("input-part1", std.os.O_RDONLY, 0) };
    var buffer : [1024]u8 = undefined;
    var expenses = std.ArrayList(u32).init(&allocator.allocator);
    defer expenses.deinit();

    var byte_buffer = std.ArrayList(u8).init(&allocator.allocator);
    defer byte_buffer.deinit();

    var read = try f.read(&buffer);
    std.log.info("Read {} bytes", .{read});
    while (read != 0) {
        for (buffer) | v, k | {
            if (std.ascii.isDigit(v)) {
                try byte_buffer.append(v);
            }
            if (v == '\n') {
                var x = atoi(byte_buffer.items[0..]);
                //std.log.debug("{}", .{x});
                try expenses.append(x);
                // Set the position back to zero, without freeing existing memory
                byte_buffer.deinit();
                byte_buffer = std.ArrayList(u8).init(&allocator.allocator);
            }
        }
        read = try f.read(&buffer);
        //std.log.info("Read {} bytes", .{read});
    }
    //std.log.info("{}", .{expenses.items.len});
    part1(expenses.items[0..]);
    part2(expenses.items[0..]);
}

fn part2(expenses : []u32) void {
    var i : u32 = 0;
    var elements : [3]u32 = undefined;
    while (i < expenses.len) : (i += 1) {
        var j : u32 = 0;
        while (j < expenses.len) : (j += 1) {
            var k : u32 = 0;
            while (k < expenses.len) : (k += 1) {
                if (k == j or k == 1) {
                    continue;
                }
                if (expenses[i] + expenses[j] + expenses[k] == 2020) {
                    elements[0] = expenses[i];
                    elements[1] = expenses[j];
                    elements[2] = expenses[k];
                }
            }
        }
    }
    std.log.info("{} and {} and {} sum to {} and multiply to {}",
                 .{elements[0], elements[1], elements[2], elements[0] + elements[1] + elements[2],
                   elements[0] * elements[1] * elements[2]});
}

fn part1(expenses : []u32) void {
    var i : u32 = 0;
    var elements : [2]u32 = undefined;
    while (i < expenses.len) : (i += 1) {
        var j : u32 = 0;
        while (j < expenses.len) : (j += 1) {
            if (i == j) {
                continue;
            }
            if (expenses[i] + expenses[j] == 2020) {
                elements[0] = expenses[i];
                elements[1] = expenses[j];
            }
        }
    }
    std.log.info("{} and {} sum to {} and multiply to {}",
                 .{elements[0], elements[1], elements[0] + elements[1],
                   elements[0] * elements[1]});
}

fn atoi(a: []u8) u32 {
    var i : u32 = 0;
    for(a) |v, k| {
        if (! std.ascii.isDigit(v)) {
            std.log.warn("Byte {x} is not a digit", .{v});
            continue;
        }
        // 48 is '0' in ascii
        std.debug.assert(v >= 48 and v < 58);
        i += @as(u32, (v - 48) * std.math.pow(u32, 10, @intCast(u32, a.len - k - 1)));
    }
    //std.log.debug("{x} --> {}", .{a, i});
    return i;
}
