const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    var gpa = &arena.allocator;

    var record_indexes = std.ArrayList(usize).init(gpa);

    var f = std.fs.File { .handle = try std.os.open("input", std.os.O_RDONLY, 0)};
    var reader = f.reader();

    var records = std.ArrayList([]u8).init(gpa);
    var record = std.ArrayList(u8).init(gpa);
    var n_newlines : u32 = 0;
    var just_had_newline = false;
    const state = enum {
        AddingData,
        CheckingNewline,
        RecordEnd,
    };
    var current_state = state.AddingData;
    while(true) {
        var n = reader.readByte() catch |err| {
            // Finalize record
            try records.append(try gpa.dupe(u8, record.items[0..record.items.len]));
            //std.log.debug("Found record: '{}'", .{record.items});
            record.deinit();
            record = std.ArrayList(u8).init(gpa);
            std.mem.set(u8, record.items, 0);
            break;
        };
        if (n == '\n') {
            if (current_state == state.CheckingNewline) {
                current_state = state.RecordEnd;
                // Finalize record
                try records.append(try gpa.dupe(u8, record.items[0..record.items.len]));
                //std.log.debug("Found record: '{}'", .{record.items});
                record.deinit();
                record = std.ArrayList(u8).init(gpa);
                std.mem.set(u8, record.items, 0);
                current_state = state.AddingData;
                continue;
            }
            else {
                current_state = state.CheckingNewline;
                // whitespace can always be added
                try record.append(' ');
                continue;
            }
        }
        else {
            if (current_state == state.CheckingNewline) {
                current_state = state.AddingData;
            }
            try record.append(n);
        }
    }
    std.log.debug("Found {} records", .{records.items.len});
    //std.log.debug("{}", .{records.items[0]});
    //const fields = comptime std.meta.fieldNames(RequiredFields);
    const fields = comptime std.meta.fields(RequiredFields);
    const valid_passport = RequiredFields {
        .byr = true,
        .iyr = true,
        .eyr = true,
        .hgt = true,
        .hcl = true,
        .ecl = true,
        .pid = true,
    };
    var n_valid : u32 = 0;
    for (records.items) | r, k | {
        // Everything is separated by whitespace
        var passport_info = RequiredFields{};
        var iter = std.mem.tokenize(r, " ");
        //std.log.debug("{}", .{r});
        while (iter.next()) |aspect| {
            inline for (fields) |field| {
                if (std.mem.startsWith(u8, aspect, field.name)) {
                    @field(passport_info, field.name) = validate_field(field.name, aspect[4..]);
                    std.log.debug("Field {} with contents {} is {}",
                                  .{field.name, aspect[4..], @field(passport_info, field.name)});
                }
            }
        }
        if (std.meta.eql(passport_info, valid_passport)) {
            std.log.debug("'{}' is valid", .{r});
            n_valid += 1;
        }
        else {
            std.log.debug("'{}' is invalid", .{r});
        }
    }
    std.log.info("Valid passports: {}", .{n_valid});
}

pub fn validate_field(field: []const u8, data: []const u8) bool {
    if (std.mem.eql(u8, field, "byr")) {
        if (data.len != 4) {
            return false;
        }
        var i = atoi(data);
        //std.log.debug("byr {} --> {}", .{data, i});
        return (i >= 1920 and i <= 2002);
    }
    if (std.mem.eql(u8, field, "iyr")) {
        if (data.len != 4) {
            return false;
        }
        var i = atoi(data);
        //std.log.debug("iyr {} --> {}", .{data, i});
        return (i >= 2010 and i <= 2020);
    }
    if (std.mem.eql(u8, field, "eyr")) {
        if (data.len != 4) {
            return false;
        }
        var i = atoi(data);
        //std.log.debug("eyr {} --> {}", .{data, i});
        return (i >= 2020 and i <= 2030);
    }
    if (std.mem.eql(u8, field, "hgt")) {
        var unit = data[data.len-2..];
        var i = atoi(data[0..data.len-2]);
        //std.log.debug("hgt {} --- > {} '{}'", .{data, i, unit});
        if (std.mem.eql(u8, unit, "in")) {
            return (i >= 59 and i <= 76);
        }
        else if (std.mem.eql(u8, unit, "cm")) {
            return (i >= 150 and i <= 193);
        }
        else {
            return false;
        }
    }
    if (std.mem.eql(u8, field, "hcl")) {
        if (data.len != 7) {
            return false;
        }
        if (data[0] != '#') {
            return false;
        }
        for (data[1..]) |d| {
            // less than ascii 0
            if (d < 48) return false;
            // greater than ascii f
            if (d > 102) return false;
            // after ascii 9 but before ascii a
            if (d > 57 and d < 97) return false;
        }
        return true;
    }
    if (std.mem.eql(u8, field, "ecl")) {
        if (data.len != 3) {
            return false;
        }
        for (valid_eyecolours) | colour | {
            if (std.mem.eql(u8, colour, data)) {
                return true;
            }
        }
        return false;
    }
    if (std.mem.eql(u8, field, "pid")) {
        if (data.len != 9) {
            return false;
        }
        for (data) |d| {
            if (!std.ascii.isDigit(d)) {
                return false;
            }
        }
        return true;
    }
    if (std.mem.eql(u8, field, "cid")) {
        return true;
    }
    std.log.debug("Uknown field {}", .{field});
    return false;
}

const valid_eyecolours = [_][]const u8 {
    "amb",
    "blu",
    "brn",
    "gry",
    "grn",
    "hzl",
    "oth",
};

fn atoi(a: []const u8) u32 {
    var i : u32 = 0;
    for(a) |v, k| {
        if (! std.ascii.isDigit(v)) {
            std.log.warn("Byte {x} is not a digit", .{v});
            continue;
        }
        // 48 is '0' in ascii
        std.debug.assert(v >= 48 and v < 58);
        i += @as(u32, (v - 48) * std.math.pow(u32, 10, @intCast(u32, a.len - k - 1)));
    }
    //std.log.debug("{x} --> {}", .{a, i});
    return i;
}
pub const RequiredFields = packed struct {
    byr: bool = false,
    iyr: bool = false,
    eyr: bool = false,
    hgt: bool = false,
    hcl: bool = false,
    ecl: bool = false,
    pid: bool = false,
};
