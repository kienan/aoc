const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    var gpa = &arena.allocator;
    defer arena.deinit();

    // Read entire file
    var f = try std.fs.cwd().openFile("input", .{});
    var contents = try f.readToEndAlloc(gpa, std.math.maxInt(u32));

    var highest_id : u32 = 0;
    var seat_ids : [814]u32 = undefined;
    var it = std.mem.tokenize(contents, "\n");
    var i : u32  = 0;
    while (it.next()) |n| {
        var s = decode_seat_id(n);
        var id = s.get_id();
        highest_id = std.math.max(highest_id, id);
        seat_ids[i] = id;
        i += 1;
    }
    std.log.info("Highest seat id: {}", .{highest_id});

    // Part2: This works because the IDs are otherwise contiguous
    std.sort.insertionSort(u32,  seat_ids[0..], {}, asc_u32);
    // Search for a gap of 3, we're the middle of the gap
    var prev : u32 = 0;
    for (seat_ids) |id| {
        //std.log.debug("{} [prev: {} // delta {}]", .{id, prev, id - prev});
        if (prev == 0) {
            prev = id;
            continue;
        }
        if ((id - prev) != 1) {
            std.log.info("Gap of {} found between {} and {}", .{id - prev, prev, id});
            std.log.info("Seat ID: {}", .{id - 1});
        }
        prev = id;
    }
}

const asc_u32 = std.sort.asc(u32);
const Seat = struct {
    row : u32 = 0,
    col : u32 = 0,

    pub fn get_id(self: *Seat) u32 {
        return (self.row*8) + self.col;
    }
};

fn decode_seat_id(code: []const u8) Seat {
    //std.log.debug("{}", .{code});
    return Seat {
        .row = _bsp(code[0..7], 127),
        .col = _bsp(code[7..], 7),
    };
}

fn _bsp(code: []const u8, max_val: u32) u32 {
    var min : u32 = 0;
    var max : u32 = max_val;
    for (code) |c| {
        var half_range = (max-min)/2;
        if (c == 'F' or c == 'L') {
            // Low half
            max = min + half_range;
        }
        else if (c == 'B' or c == 'R') {
            min = max - half_range;
        }
    }
    //std.log.debug("{} --> {}, {}", .{code, min, max});
    std.debug.assert(max == min);
    return max;
}

test "bsp" {
    var s0 = decode_seat_id("FBFBBFFRLR");
    std.testing.expectEqual(s0.row, 44);
    std.testing.expectEqual(s0.col, 5);
    std.testing.expectEqual(s0.get_id(), 357);

    var s1 = decode_seat_id("BFFFBBFRRR");
    std.testing.expectEqual(s1.row, 70);
    std.testing.expectEqual(s1.col, 7);
    std.testing.expectEqual(s1.get_id(), 567);

    var s2 = decode_seat_id("FFFBBBFRRR");
    std.testing.expectEqual(s2.row, 14);
    std.testing.expectEqual(s2.col, 7);
    std.testing.expectEqual(s2.get_id(), 119);

    var s3 = decode_seat_id("BBFFBBFRLL");
    std.testing.expectEqual(s3.row, 102);
    std.testing.expectEqual(s3.col, 4);
    std.testing.expectEqual(s3.get_id(), 820);
}
