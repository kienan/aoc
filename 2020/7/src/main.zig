const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    var gpa = &arena.allocator;

    var f = try std.fs.cwd().openFile("input", .{});
    var contents = try f.readToEndAlloc(gpa, std.math.maxInt(u32));

    var next_id : u32 = 0;
    var map = std.hash_map.StringHashMap(u32).init(gpa);
    var it = std.mem.tokenize(contents, "\n");
    var bag_rules = std.ArrayList(*BagType).init(gpa);
    defer bag_rules.deinit();

    while (it.next()) |line| {
        //std.log.debug("{}", .{line});
        var bag = try gpa.create(BagType);
        bag.*.contains = std.ArrayList(*BagRule).init(gpa);

        var need_desc = true;
        var desc = std.ArrayList(u8).init(gpa);
        defer desc.deinit();
        var need_count = true;
        var count: u32 = 0;
        for (line) |a| {
            if (need_desc) {
                try desc.append(a);
                if (desc.items.len > 5) {
                    if (std.mem.eql(u8, " bag", desc.items[desc.items.len-4..])) {
                        //std.log.debug("'{}'", .{desc.items});
                        if (!need_count) {
                            var key = try std.mem.dupe(gpa, u8, std.mem.trim(u8, desc.items[0..desc.items.len-4], " "));
                            var bag_id : u32 = 0;
                            //std.log.debug("'{} {} bags'", .{count, key});
                            if (map.contains(key)) {
                                bag_id = map.get(key).?;
                                //std.log.debug("Bag '{}': {}", .{key, bag_id});
                            }
                            else {
                                bag_id = next_id;
                                //std.log.debug("Assigned ID {} to '{}'", .{bag_id, key});
                                try map.putNoClobber(key, next_id);
                                next_id += 1;
                            }
                            var br = try gpa.create(BagRule);
                            br.count = count;
                            br.bag_type = bag_id;
                            std.log.debug("Bag {} should contain {} of {}", .{bag.desc, br.count, br.bag_type});
                            try bag.contains.append(br);
                        }
                        else {
                            var key = try std.mem.dupe(gpa, u8, desc.items[0..desc.items.len-4]);
                            var bag_id : u32 = 0;
                            bag.desc = key;
                            //std.log.debug("'{}'", .{bag.desc});
                            if (map.contains(key)) {
                                bag_id = map.get(key).?;
                                //std.log.debug("Bag '{}': {}", .{key, bag_id});
                            }
                            else {
                                bag_id = next_id;
                                //std.log.debug("Assigned ID {} to '{}'", .{bag_id, key});
                                try map.putNoClobber(key, next_id);
                                next_id += 1;
                            }
                            bag.type_id = bag_id;
                        }
                        need_desc = false;
                        need_count = true;
                        desc.deinit();
                        desc = std.ArrayList(u8).init(gpa);
                    }
                }
            }
            else if (need_count) {
                if (std.ascii.isDigit(a)) {
                    count = @as(u32, a - 48);
                    need_count = false;
                    need_desc = true;
                }
            }
        }
        //std.log.debug("{}", .{bag});
        try bag_rules.append(bag);
    }
    // for (bag_rules.items) |rule| {
    //     std.log.debug("{} ({})", .{rule.desc.?, rule.type_id});
    //     for(rule.contains.items) |r| {
    //         std.log.debug("\t{} of {}", .{r.count, r.bag_type});
    //     }
    // }
    var shiny_gold_id = map.get("shiny gold");
    std.log.debug("shiny gold: {}", .{shiny_gold_id});

    //var types_that_can_contain_shiny_gold = try types_that_can_contain(gpa, &bag_rules, shiny_gold_id.?);
    //std.log.info("{} bag types can contain shiny gold bags ({})", .{types_that_can_contain_shiny_gold.len, shiny_gold_id});

    var shiny_gold_type = get_type_by_id(&bag_rules, shiny_gold_id.?).?;
    std.log.debug("shiny gold type has {} contain rules", .{shiny_gold_type.*.contains.items.len});
    //std.log.debug("{}", .{shiny_gold_rule});
    var n_bags_needed = bag_type_has_how_many_bags_needed(shiny_gold_type, &bag_rules);
    std.log.info("{} needs {} bags purchased, including itself", .{shiny_gold_type.*.desc, n_bags_needed});
}

fn bag_type_has_how_many_bags_needed(self: *BagType, list: *std.ArrayList(*BagType)) u32 {
    var count : u32 = 1;
    if (self.contains.items.len == 0) {
        // We just need our self
        std.log.debug("{} just has itself", .{self.type_id});
        return count;
    }
    std.log.debug("{}", .{self.contains.items.len});
    for (self.contains.items) |i| {
        if (i.*.count == 0) {
            continue;
        }
        var bagtype = get_type_by_id(list, i.*.bag_type).?;
        var extra = (i.*.count * bag_type_has_how_many_bags_needed(bagtype, list));
        std.log.debug("{} needs {}x {} --> adding {} bags", .{self.*.desc, i.*.count, bagtype.*.desc, extra});
        count += extra;
    }
    return count;
}

fn get_type_by_id(list: *std.ArrayList(*BagType), id: u32) ?*BagType {
    for (list.items) |r| {
        if (r.*.type_id == id) {
            return r;
        }
    }
    return null;
}

// fn types_that_can_contain(allocator: *std.mem.Allocator, list: *std.ArrayList(BagType), id: u32) anyerror![]u32 {
//     var search_ids = std.ArrayList(u32).init(allocator);
//     defer search_ids.deinit();
//     for (list.items) |r| {
//         if (r.can_contain(id)) {
//             //std.log.debug("{} can contain {}", .{r.type_id, id});
//             try search_ids.append(r.type_id);
//         }
//     }
//     var count : usize = search_ids.items.len;
//     //std.log.debug("{} bag types can directly contain a {}",
//     //              .{count, id});
//     var types = std.ArrayList(u32).init(allocator);
//     for (search_ids.items) |i| {
//         var t = try types_that_can_contain(allocator, list, i);
//         //std.log.debug("Adding {} to types that can hold {}", .{i, id});
//         try types.append(i);
//         for (t) |tid| {
//             var already_in_types = false;
//             for (types.items) |asdf| {
//                 if (asdf == tid) {
//                     already_in_types = true;
//                     break;
//                 }
//             }
//             if (!already_in_types) {
//                 //std.log.debug("Adding {} to types that can hold {}", .{tid, id});
//                 try types.append(tid);
//             }
//         }
//     }
//     return types.items[0..];
// }

const BagRule = struct {
    bag_type: u32 = 0,
    count: u32 = 0,
};

const BagType = struct {
    type_id: u32 = 0,
    desc: ?[]u8 = null,
    contains: std.ArrayList(*BagRule),

    pub fn can_contain(self: *const BagType, type_id: u32) bool {
        for (self.contains.items) |rule| {
            if (rule.*.bag_type == type_id and rule.*.count > 0) {
                return true;
            }
        }
        return false;
    }
};
