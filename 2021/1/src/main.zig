const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);

    // Part 1
    var depths = std.ArrayList(u32).init(alloc);
    defer depths.deinit();

    var it = std.mem.tokenize(contents, "\n");
    var max: u32 = 0;
    var increases: u32 = 0;
    while (it.next()) |line| {
        var v = try std.fmt.parseInt(u32, line, 10);
        // This will always happen
        defer max = v;
        // Fill in the depths for part2
        try depths.append(v);
        if (max == 0) {
            continue;
        }
        if (v > max) {
            increases += 1;
        }
    }
    std.log.info("[Part 1] There were {} increases", .{increases});

    // Part 2
    // Working with a sliding window of 3
    var i: usize = 0;
    increases = 0;
    max = 0;
    while (i < depths.items.len - 3) : (i += 1) {
        // Current window
        var a: u32 = depths.items[i] + depths.items[i+1] + depths.items[i+2];
        var b: u32 = depths.items[i+1] + depths.items[i+2] + depths.items[i+3];
        // Visualize it
        //std.log.info("A: {} // B: {}\n{} A\n{} A B\n{} A B\n{}   B\n",
        //             .{a, b, depths.items[i], depths.items[i+1],
        //               depths.items[i+2], depths.items[i+3]});
        if (b > a) {
            increases += 1;
        }
    }
    std.log.info("[Part 2] There were {} increases", .{increases});
}
