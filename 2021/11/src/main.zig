const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);

    var map: [100]u8 = undefined;
    var lit = std.mem.tokenize(contents, "\n");
    var line_number: u16 = 0;
    while (lit.next()) |line| {
        for (line) |c, i| {
            map[i + @as(usize, line_number * 10)] = try std.fmt.parseInt(u8, line[i..i+1], 10);
        }
        line_number += 1;
    }

    var step: u16 = 0;
    var flashed = std.AutoHashMap(usize, void).init(alloc);
    defer flashed.deinit();
    var flashes: u32 = 0;
    while (step < 1000) {
        // Increase energy level of octopuses by 1
        for (map) |v, i| {
            map[i] += 1;
        }
        // Check for high energy octopi
        for (map) |v, i| {
            if (v > 9) {
                // Flash!
                try flash(i, map[0..], &flashed);
            }
        }
        //std.log.debug("Step {} had {} octopi flash", .{step, flashed.count()});
        flashes += flashed.count();
        if (flashed.count() == 100) {
            std.log.info("[Part 2] On step {}, all octopi flashed", .{step+1});
            break;
        }
        // Return all flashed octopi to 0 energy
        var fit = flashed.keyIterator();
        while (fit.next()) |fi| {
            map[fi.*] = 0;
        }
        flashed.clearRetainingCapacity();
        step += 1;
        if (step == 100) {
            std.log.info("[Part 1] After {} steps, {} octopi have flashed",
                         .{step, flashes});
        }
        // std.log.debug("After step:\n", .{});
        // var i: usize = 0;
        // while (i < 10) : (i += 1) {
        //     std.log.debug("{any}", .{map[i..i+10]});
        // }
        // std.log.debug("\n", .{});
    }
}

fn flash(i: usize, map: []u8, flashed: *std.AutoHashMap(usize, void)) anyerror!void {
    if (flashed.contains(i)) {
        return;
    }
    try flashed.putNoClobber(i, undefined);
    var adjacents = get_adjacent_points(i, 10, 10);
    for (adjacents) |adj| {
        if (adj) |a| {
            if (!flashed.contains(a)) {
                map[a] += 1;
                if (map[a] > 9) {
                    try flash(a, map, flashed);
                }
            }
        }
    }
}

fn get_adjacent_points(i: usize, width: usize, height: usize) [8]?usize {
    var adjacents = [8] ?usize {
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    };
    var ai: usize = 0;
    var bottom = i < ((height - 1) * width);
    var top = i >= width;
    var left = (i % width) != 0;
    var right = (i % width) != (width - 1);
    if (top) {
        if (left) {
            adjacents[ai] = i - width - 1;
            ai += 1;
        }
        adjacents[ai] = i - width;
        ai += 1;
        if (right) {
            adjacents[ai] = i - width + 1;
            ai += 1;
        }
    }
    if (left) {
        adjacents[ai] = i - 1;
        ai += 1;
    }
    if (right) {
        adjacents[ai] = i + 1;
        ai += 1;
    }
    if (bottom) {
        if (left) {
            adjacents[ai] = i + width - 1;
            ai += 1;
        }
        adjacents[ai] = i + width;
        ai += 1;
        if (right) {
            adjacents[ai] = i + width + 1;
            ai += 1;
        }
    }
    return adjacents;
}
