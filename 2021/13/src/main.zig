const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);

    var it = std.mem.tokenize(contents, "\n");
    var reading_folds = false;
    var map = std.AutoHashMap(Point, void).init(alloc);
    var folds = std.ArrayList(Fold).init(alloc);
    defer folds.deinit();
    defer map.deinit();
    while (it.next()) |line| {
        if (reading_folds or std.mem.eql(u8, line[0..4], "fold")) {
            reading_folds = true;
            var lit = std.mem.tokenize(line, " ");
            var l = lit.next().?;
            l = lit.next().?;
            l = lit.next().?;
            var fit = std.mem.tokenize(l, "=");
            var axis = fit.next().?[0];
            var value = try std.fmt.parseInt(u16, fit.next().?, 10);
            try folds.append(.{
                .axis = switch(axis) {
                    'x' => .x,
                    'y' => .y,
                    else => unreachable,
                },
                .value = value,
            });
        }
        else {
            var lit = std.mem.tokenize(line, ",");
            var x: u16 = try std.fmt.parseInt(u16, lit.next().?, 10);
            var y: u16 = try std.fmt.parseInt(u16, lit.next().?, 10);
            try map.put(.{.x = x, .y = y}, undefined);
        }
    }
    std.log.debug("Map has {} items", .{map.count()});
    var points_to_change = std.AutoHashMap(Point, Point).init(alloc);
    defer points_to_change.deinit();
    for (folds.items) |fold, i| {
        // We want the points where the axis field is > fold.value
        // To fold it over the axis, we get the distance of the
        // point from the fold line, then reposition it to
        // foldline - distance
        var mit = map.keyIterator();
        while (mit.next()) |k| {
            switch(fold.axis) {
                .x => {
                    if (@field(k.*, "x") > fold.value) {
                        var new_point: Point = .{};
                        @field(new_point, "x") = fold.value - (@field(k.*, "x") - fold.value);
                        @field(new_point, "y") = @field(k.*, "y");
                        try points_to_change.put(k.*, new_point);
                    }
                },
                .y => {
                    if (@field(k.*, "y") > fold.value) {
                        var new_point: Point = .{};
                        @field(new_point, "y") = fold.value - (@field(k.*, "y") - fold.value);
                        @field(new_point, "x") = @field(k.*, "x");
                        try points_to_change.put(k.*, new_point);
                    }

                },
            }
        }
        var pit = points_to_change.iterator();
        while (pit.next()) |kv| {
            _ = map.remove(kv.key_ptr.*);
            try map.put(kv.value_ptr.*, undefined);
        }
        std.log.debug("After {} folds, there are {} points", .{i+1, map.count()});
        points_to_change.clearRetainingCapacity();
    }

    // Determine max x, y for printing
    var mit = map.keyIterator();
    var max_x: u16 = 0;
    var max_y: u16 = 0;
    while (mit.next()) |p| {
        if (p.*.x > max_x) {
            max_x = p.*.x;
        }
        if (p.*.y > max_y) {
            max_y = p.*.y;
        }
    }
    std.log.debug("Max dimensions are {} x {}", .{max_x, max_y});
    var x: u16 = 0;
    var y: u16 = 0;
    const stdout = std.io.getStdOut().writer();
    while (y <= max_y) : (y += 1) {
        x = 0;
        while (x <= max_x) : (x += 1) {
            var p = Point { .x = x, .y = y };
            if (map.contains(p)) {
                try stdout.writeAll("*");
            }
            else {
                try stdout.writeAll(" ");
            }
        }
        try stdout.writeAll("\n");
    }
}

const Point = struct {
    x: u16 = 0,
    y: u16 = 0,
};
const Fold = struct {
    axis: FoldAxis = .x,
    value: u16 = 0,
    pub const FoldAxis = enum {
        x,
        y,
    };
};
