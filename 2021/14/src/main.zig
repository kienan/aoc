const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);

    var it = std.mem.tokenize(contents, "\n");
    var template: []const u8 = it.next().?;
    var replacements = std.ArrayList(Replacement).init(alloc);
    defer replacements.deinit();
    while (it.next()) |line| {
        var lit = std.mem.tokenize(line, " ");
        var pair = lit.next().?;
        _ = lit.next(); // skip
        var insert = lit.next().?;
        var r: Replacement = undefined;
        r.pair[0] = pair[0];
        r.pair[1] = pair[1];
        r.insert = insert[0];
        try replacements.append(r);
    }

    var compound = std.ArrayList(u8).init(alloc);
    defer compound.deinit();

    // Clone our template
    for (template) |t| {
        try compound.append(t);
    }

    var step: u8 = 0;
    while (step < 10) : (step += 1) {
        var index: usize = 0;
        while (index < (compound.items.len - 1)) : (index += 1) {
            
        }
    }
}

const Replacement = struct {
    pair: [2]u8,
    insert: u8,
};
