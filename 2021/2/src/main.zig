const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    // Read our input
    var f = try std.fs.cwd().openFile("input", .{});
    defer f.close();
    var contents = try f.readToEndAlloc(alloc, std.math.maxInt(u32));
    defer alloc.free(contents);

    // Part 1
    var pos_x: i32 = 0;
    var depth: i32 = 0;

    var it = std.mem.tokenize(contents, "\n");
    while (it.next()) |line| {
        var lit = std.mem.tokenize(line, " ");
        var dir = lit.next().?;
        var amount = try std.fmt.parseInt(i32, lit.next().?, 10);
        if (std.mem.eql(u8, dir, "up")) {
            depth -= amount;
        }
        else if (std.mem.eql(u8, dir, "forward")) {
            pos_x += amount;
        }
        else if (std.mem.eql(u8, dir, "down")) {
            depth += amount;
        }
    }
    std.log.info("[Part 1] Depth {}, Horizontal Position {}, Sum {}",
                 .{depth, pos_x, depth * pos_x});

    // Part 2
    pos_x = 0;
    var aim: i32 = 0;
    depth = 0;
    it = std.mem.tokenize(contents, "\n");
    while (it.next()) |line| {
        var lit = std.mem.tokenize(line, " ");
        var dir = lit.next().?;
        var amount = try std.fmt.parseInt(i32, lit.next().?, 10);
        if (std.mem.eql(u8, dir, "up")) {
            aim -= amount;
        }
        else if (std.mem.eql(u8, dir, "forward")) {
            depth += amount * aim;
            pos_x += amount;
        }
        else if (std.mem.eql(u8, dir, "down")) {
            aim += amount;
        }
    }
    std.log.info("[Part 2] Depth {}, Horizontal Position {}, Sum {}",
                 .{depth, pos_x, depth * pos_x});
}
