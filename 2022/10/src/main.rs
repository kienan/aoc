use std::str::FromStr;

fn main() {
    let input_file = common::parse_args_input_file(&mut std::env::args());
    let contents = std::fs::read_to_string(input_file).expect("Failed to read input file");
    let program = parse_program(&contents);
    let (result, screen) = run_program(&program);
    println!("[PART 1]: {}", result);
    println!("");
    for (index, value) in screen.iter().enumerate() {
        let c = match value {
            true => '#',
            false => '.',
        };
        print!("{}", c);
        if (index + 1) % 40 == 0 {
            println!("");
        }
    }
}

#[derive(Clone)]
enum Op {
    Noop,
    Addx,
}

#[derive(Clone)]
struct Instruction {
    op: Op,
    cycles: i32,
    arg: Option<i32>,
}

impl Instruction {
    fn from_str(s: &str) -> Instruction {
        let mut i = Instruction {
            op: Op::Noop,
            cycles: 1,
            arg: None,
        };
        let words: std::vec::Vec<&str> = s.split(' ').collect();
        if words.len() > 1 {
            match words[0] {
                "addx" => {
                    i.cycles = 2;
                    i.op = Op::Addx;
                    i.arg = Some(i32::from_str(words[1]).expect("Failed to parse int"));
                },
                _ => {
                    unreachable!();
                },
            };
        }
        return i;
    }
}

fn parse_program(lines: &String) -> std::vec::Vec<Instruction> {
    let mut program = std::vec::Vec::<Instruction>::new();
    for line in lines.lines() {
        if line.eq("") {
            continue;
        }
        program.push(Instruction::from_str(line));
    }
    return program;
}

fn run_program(program: &std::vec::Vec<Instruction>) -> (i32, std::vec::Vec<bool>) {
    let mut p = program.clone();
    p.reverse();
    let mut cycles = 0;
    let mut reg_x  = 1;
    let mut result = 0;
    let mut screen = std::vec::Vec::<bool>::new();
    loop {
        cycles += 1;
        let mut instruction = p.last_mut();
        if instruction.is_none() {
            break;
        }
        instruction.as_mut().unwrap().cycles -= 1;
        // Check signal strength
        // println!("Cycle {}: register x {}", cycles, reg_x);
        if (cycles - 20) % 40 == 0 {
            let v = cycles * reg_x;
            println!("Cycle {}: signal value {} changes result from {} to {}",
                    cycles, v, result, result + v);
            result += v;
        }
        // Draw to screen
        // @BUG - The screen (or the screen printing routine) has an off by one error
        // Theoutput of the program was

        // ###...##..###..#..#.####.#..#.####...##.
        // ...#.#..#.#..#.#.#..#....#.#..#.......#.
        // ...#.#..#.#..#.##...###..##...###.....#.
        // ###..####.###..#.#..#....#.#..#.......#.
        // .....#..#.#....#.#..#....#.#..#....#..#.
        // .....#..#.#....#..#.#....#..#.####..##..

        // The screen is supposed to display 8 characters, and the first has a column
        // cut off; however, given the shape I was able to deduce the answer for the
        // purpose of the puzzle and I don't feel like fixing the routines at this
        // time.

        if reg_x < 0 {
            // Sprite is not visible, draw dark
            screen.push(false);
        }
        else {
            // Sprite may be  visible
            let sprite_pos = reg_x % 40;
            let pixel_number = (screen.len() % 40) as i32;
            if pixel_number >= sprite_pos - 1 && pixel_number <= sprite_pos + 1 {
                screen.push(true);
            }
            else {
                screen.push(false);
            }
        }
        if instruction.as_ref().unwrap().cycles <= 0 {
            match instruction.as_ref().unwrap().op {
                Op::Noop => {},
                Op::Addx => {
                    reg_x += instruction.unwrap().arg.unwrap();
                },
            };
            _ = p.pop();
        }
    }
    return (result, screen);
}
