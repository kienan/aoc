use std::str::FromStr;

fn main() {
    let input_file = common::parse_args_input_file(&mut std::env::args());
    let contents = std::fs::read_to_string(input_file).expect("Failed to read input file");
    let mut p0: Option<PacketData> = None;
    let mut p1: Option<PacketData> = None;
    let mut pair_count = 1;
    let mut part1_sum = 0;
    let mut packets = std::vec::Vec::<PacketData>::new();
    for line in contents.lines() {
        if line.eq("") {
            p0 = None;
            p1 = None;
            continue;
        }
        packets.push(PacketData::from_string(line).unwrap());
        if p0.as_ref().is_none() {
            p0 = PacketData::from_string(line);
        }
        else if p1.as_ref().is_none() {
            p1 = PacketData::from_string(line);
        }
        if p0.as_ref().is_some() && p1.as_ref().is_some() {
            let mut m = false;
            if p0.as_ref().unwrap().compare_ok(p1.as_ref().unwrap()) == Comparison::Okay {
                part1_sum += pair_count;
                m = true;
            }
            // println!("{}\n{}\n{}\n", p0.as_ref().unwrap().to_string(), p1.as_ref().unwrap().to_string(), m);
            pair_count += 1;
        }
    }
    println!("[PART 1] {}", part1_sum);

    let divider0 = PacketData::from_string("[[2]]").unwrap();
    let divider1 = PacketData::from_string("[[6]]").unwrap();
    packets.push(divider0.clone());
    packets.push(divider1.clone());
    packets.sort_by(|a, b| a.compare(b));
    let mut divider0_index = 0;
    let mut divider1_index = 0;
    for (index, p) in packets.iter().enumerate() {
        println!("{}", p.to_string());
        if divider0.compare(p) == std::cmp::Ordering::Equal {
            divider0_index = index;
        }
        if divider1.compare(p) == std::cmp::Ordering::Equal {
            divider1_index = index;
        }
    }
    println!("[PART 2] {}", (divider0_index + 1) * (divider1_index + 1));
}

#[derive(Clone,Debug)]
enum PacketData {
    Value(u32),
    List(std::vec::Vec<Self>),
}

#[derive(Debug,PartialEq)]
enum Comparison {
    Okay,
    NotOkay,
    Unsure,
}

impl PacketData {

    pub fn len(&self) -> usize {
        return match self {
            PacketData::Value(_) => { 0 },
            PacketData::List(v) => { v.len() },
        };
    }

    pub fn is_list(&self) -> bool {
        return match self {
            PacketData::Value(_) => { false },
            PacketData::List(_) => { true },
        };
    }

    pub fn compare(&self, other: &PacketData) -> std::cmp::Ordering {
        let r = self.compare_ok(other);
        return match r {
            Comparison::Okay => { std::cmp::Ordering::Less },
            Comparison::Unsure => { std::cmp::Ordering::Equal },
            Comparison::NotOkay => { std::cmp::Ordering::Greater },
        };
    }

    pub fn compare_ok(&self, other: &PacketData) -> Comparison {
        let mut result = Comparison::Unsure;
        if !self.is_list() && !other.is_list() {
            let left = match self {
                PacketData::Value(v) => { v },
                _ => { unreachable!(); },
            };
            let right = match other {
                PacketData::Value(v) => { v },
                _ => { unreachable!(); },
            };
            if left < right {
                result = Comparison::Okay;
            }
            else if left > right {
                result = Comparison::NotOkay;
            }
        }
        else if self.is_list() && other.is_list() {
            let left = match self {
                PacketData::List(v) => { v },
                _ => { unreachable!(); },
            };
            let right = match other {
                PacketData::List(v) => { v },
                _ => { unreachable!(); },
            };
            let left_len = left.len();
            let right_len = right.len();
            for index in 0..std::cmp::max(left_len, right_len) {
                let left_item = left.get(index);
                let right_item = right.get(index);
                if right_item.is_none() && left_item.is_some() {
                    // println!("right ran out of items first");
                    result = Comparison::NotOkay;
                    break;
                }
                if left_item.is_none() && right_item.is_some() {
                    result = Comparison::Okay;
                    break;
                }
                let r = left[index].compare_ok(&right[index]);
                if r != Comparison::Unsure {
                    result = r;
                    break;
                }
            }
        }
        else {
            if !self.is_list() {
                // Convert self to list
                let mut list = std::vec::Vec::<PacketData>::new();
                list.push(self.clone());
                result = PacketData::List(list).compare_ok(other);
            }
            else {
                // Convert other to list
                let mut list = std::vec::Vec::<PacketData>::new();
                list.push(other.clone());
                result = self.compare_ok(&PacketData::List(list));
            }
        }
        // println!("{} vs {}: {:?}", self.to_string(), other.to_string(), result);
        return result;
    }

    pub fn to_string(&self) -> String {
        let mut s = String::new();
        self._to_string(&mut s);
        return s;
    }

    fn _to_string(&self, s: &mut String) {
        match self {
            PacketData::Value(v) => {
                s.push_str(&v.to_string());
            },
            PacketData::List(v) => {
                s.push('[');
                let len = v.len();
                for (index, item) in v.iter().enumerate() {
                    item._to_string(s);
                    if index != v.len() - 1 {
                        s.push(',');
                    }
                }
                s.push(']');
            }
        };
    }
    pub fn from_string(s: &str) -> Option<PacketData> {
        if s.len() == 0 {
            return None;
        }
        let (p, parsed_count) = PacketData::_from_bytes(s.as_bytes());
        assert_eq!(parsed_count, s.len());
        return p;
    }

    fn _from_bytes(bytes: &[u8]) -> (Option<PacketData>, usize) {
        if bytes.len() == 0 {
            return (None, 0);
        }
        let list = bytes[0] == b'[';
        if list {
            let mut p = PacketData::List(std::vec::Vec::<PacketData>::new());
            let mut start = 1;
            let mut end = 1;
            let mut open_count = 1;
            for index in start..bytes.len() {
                if bytes[index] == b'[' {
                    open_count += 1;
                }
                else if bytes[index] == b']' {
                    open_count -= 1;
                    if open_count == 0 {
                        end = index;
                        break;
                    }
                }
            }
            let mut count = 2;
            let mut sub_str = &bytes[start..end];
            if sub_str.len() == 0 {
                // Empty list
                return (Some(p), count);
            }
            else {
                while start < end {
                    let (element, advance) = PacketData::_from_bytes(sub_str);
                    count += advance;
                    assert!(element.is_some());
                    // println!("{}, {:?}", advance, element);
                    match p {
                        PacketData::List(ref mut v) => {
                            v.push(element.unwrap());
                        },
                        _ => {},
                    };
                    // Update start and end positions?
                    start += advance;
                    // Peek at the next byte, if it's a comma,
                    // we can increment the start and count
                    if bytes[start] == b',' {
                        start += 1;
                        count += 1;
                    }
                    sub_str = &bytes[start..end];
                    // println!("{}, {}, count {} of {}. next: {:?}", start, end, count, end, sub_str);
                }
            }
            return (Some(p), count);
        }
        else {
            let start = 0;
            let mut end = 0;
            for index in start..bytes.len() {
                if bytes[index] > 47 && bytes[index] < 58 {
                    // Is an ascii numeral from 0 to 9
                    end += 1;
                }
                else {
                    break;
                }
            }
            assert!(start != end);
            let s = std::str::from_utf8(&bytes[start..end]).expect("Couldn't parse bytes to utf8");
            let v = u32::from_str(s).expect("Couldn't parse string to u32");
            return (Some(PacketData::Value(v)), end - start);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_simple_value() {
        let p = PacketData::from_string("1233");
        assert!(p.is_some());
        match p.unwrap() {
            PacketData::Value(v) => {
                assert_eq!(v, 1233);
            },
            PacketData::List(v) => {
                assert!(false);
            }
        };
    }

    #[test]
    fn parse_empty() {
        let p = PacketData::from_string("[]");
        assert!(p.is_some());
        match p.unwrap() {
            PacketData::Value(v) => { assert!(false); },
            PacketData::List(v) => {
                assert_eq!(v.len(), 0);
            }
        }
    }

    #[test]
    fn parse_simple_list() {
        let p = PacketData::from_string("[1,2,3,4,5]");
        assert!(p.is_some());
        match p.unwrap() {
            PacketData::Value(v) => { assert!(false); },
            PacketData::List(v) => {
                assert_eq!(v.len(), 5);
            }
        }
    }

    #[test]
    fn parse_recursive() {
        let mut source = "[[1],4]";
        let mut p = PacketData::from_string(source);
        assert!(p.is_some());
        match p.as_ref().unwrap() {
            PacketData::Value(v) => { assert!(false); },
            PacketData::List(v) => {
                assert_eq!(v.len(), 2);
            }
        }
        assert_eq!(source, p.unwrap().to_string());

        source = "[1,[2,[3,[4,[5,6,0]]]],8,9]";
        p = PacketData::from_string(source);
        assert!(p.is_some());
        match p.as_ref().unwrap() {
            PacketData::Value(v) => { assert!(false); },
            PacketData::List(v) => {
                assert_eq!(v.len(), 4);
            }
        }
        assert_eq!(source, p.unwrap().to_string());
    }
}
