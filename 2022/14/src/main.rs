use std::str::FromStr;
use common::Point;

fn main() {
    let input_file = common::parse_args_input_file(&mut std::env::args());
    let contents = std::fs::read_to_string(input_file).expect("Failed to read input file");
    let mut cave = Cave::new();
    cave.parse_lines(&contents);
    let mut cave2 = Cave::new();
    cave2.parse_lines(&contents);
    let mut iteration = 0;
    loop {
        let s = cave.add_sand();
        if s == FallingState::Abyss {
            break;
        }
        iteration += 1;
    }
    println!("[PART 1] {}", iteration);

    cave2.floor = Some(cave2.lowest_rock_height - 2);
    iteration = 0;
    loop {
        let s = cave2.add_sand();
        if s == FallingState::Blocked {
            break;
        }
        iteration += 1;
    }
    println!("[PART 2] {}", iteration);
}

#[derive(Clone,Copy,Debug)]
enum Object {
    Rock,
    Sand,
}

#[derive(Debug,PartialEq)]
enum FallingState {
    Abyss,
    Blocked,
    Moved,
    Rest,
}

struct Cave {
    sand_source: Point,
    lowest_rock_height: i32,
    floor: Option<i32>,
    data: std::collections::HashMap<Point, Object>,
}

impl Cave {
    pub fn new() -> Cave {
        return Cave {
            sand_source: Point{ x: 500, y: 0 },
            lowest_rock_height: 0,
            floor: None,
            data: std::collections::HashMap::<Point, Object>::new(),
        };
    }

    fn add_rock_line(&mut self, a: &Point, b: &Point) {
        let mut p = a.clone();
        //self.data.insert(*a, Object::Rock);
        let mut delta = p.step_towards(b);
        while delta != (Point { x: 0, y: 0 }) {
            self.data.insert(p, Object::Rock);
            self.lowest_rock_height = std::cmp::min(self.lowest_rock_height, p.y);
            p.add(&delta);
            delta = p.step_towards(b);
        }
        self.data.insert(p, Object::Rock);
        self.lowest_rock_height = std::cmp::min(self.lowest_rock_height, p.y);
    }

    fn parse_lines(&mut self, data: &String) {
        for line in data.lines() {
            if line.eq("") {
                continue;
            }
            let words: std::vec::Vec<&str> = line.split_terminator("->").collect();
            for index in 1..words.len() {
                let (ax, ay) = words[index - 1].trim().split_once(',').unwrap();
                let (bx, by) = words[index].trim().split_once(',').unwrap();
                self.add_rock_line(
                    &Point { x: i32::from_str(ax).unwrap(), y: -i32::from_str(ay).unwrap() },
                    &Point { x: i32::from_str(bx).unwrap(), y: -i32::from_str(by).unwrap() }
                );
            }
        }
    }

    fn add_sand(&mut self) -> FallingState {
        let mut position = self.sand_source.clone();
        if self.data.contains_key(&position) {
            return FallingState::Blocked;
        }
        loop {
            let options = [
                Point { x: position.x, y: position.y - 1 },
                Point { x: position.x - 1, y: position.y - 1 },
                Point { x: position.x + 1, y: position.y - 1 },
            ];
            if self.floor.is_some() && self.floor.unwrap() == (position.y - 1) {
                // Come to rest because we're on the floor
                self.data.insert(position, Object::Sand);
                return FallingState::Rest;
            }
            let mut x: Option<usize> = None;
            for (index, point) in options.iter().enumerate() {
                if !self.data.contains_key(point) {
                    x = Some(index);
                    break;
                }
            }
            if x.is_none() {
                // Can't move, settle here
                self.data.insert(position, Object::Sand);
                return FallingState::Rest;
            }
            else {
                position = options[x.unwrap()];
                if self.floor.is_none() && position.y < self.lowest_rock_height {
                    return FallingState::Abyss
                }
            }
        }
    }
}
