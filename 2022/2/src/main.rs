use common;

mod outcome;
mod shape;

use outcome::Outcome;
use shape::Shape;

fn main() {
    let input_file = common::parse_args_input_file(&mut std::env::args());
    let contents = std::fs::read_to_string(input_file).expect("Couldn't read contents of input file");

    let mut plays = std::vec::Vec::<Play>::new();
    for line in contents.lines() {
        if line.eq("") {
            continue;
        }
        let chars: std::vec::Vec<char> = line.chars().collect();
        assert_eq!(chars.len(), 3);
        plays.push(Play {
            opponent: Shape::from_hint(chars[0]),
            hinted_response: Shape::from_hint(chars[2]),
            hinted_outcome: Outcome::from_hint(chars[2]),
        });
    }
    let mut score: u32 = 0;
    for play in &plays {
        score += play.hinted_response.score(&play.opponent);
    }
    println!("[PART 1] Score if played as suggested: {}", score);

    score = 0;
    for play in &plays {
        let shape = play.opponent.find_shape_to_achieve_outcome(&play.hinted_outcome);
        score += shape.score(&play.opponent);
    }
    println!("[PART 2] Score if played with hinted outcomes: {}", score);
}

#[derive(Debug)]
struct Play {
    opponent: Shape,
    hinted_response: Shape,
    hinted_outcome: Outcome,
}
