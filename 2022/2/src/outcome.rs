#[derive(Debug, PartialEq)]
pub enum Outcome {
    Loss,
    Draw,
    Win,
}

impl Outcome {
    pub fn from_hint(c: char) -> Self {
        return match c {
            'X' => Outcome::Loss,
            'Y' => Outcome::Draw,
            'Z' => Outcome::Win,
            _ => unreachable!(),
        };
    }

    pub fn value(&self) -> u32 {
        return match self {
            Self::Loss => 0,
            Self::Draw => 3,
            Self::Win => 6,
        };
    }
}
