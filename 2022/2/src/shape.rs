use crate::Outcome;

#[derive(Clone, Debug, PartialEq)]
pub enum Shape {
    Rock,
    Paper,
    Scissors,
}

impl Shape {
    pub fn find_shape_to_achieve_outcome(&self, outcome: &Outcome) -> Self {
        return match outcome {
            Outcome::Draw => self.clone(),
            Outcome::Win => {
                match self {
                    Shape::Rock => Shape::Paper,
                    Shape::Paper => Shape::Scissors,
                    Shape::Scissors => Shape::Rock,
                }
            },
            Outcome::Loss => {
                match self {
                    Shape::Rock => Shape::Scissors,
                    Shape::Paper => Shape::Rock,
                    Shape::Scissors => Shape::Paper,
                }
            },
        };
    }

    pub fn from_hint(hint: char) -> Self {
        return match hint {
            'A' | 'X' => Shape::Rock,
            'B' | 'Y' => Shape::Paper,
            'C' | 'Z' => Shape::Scissors,
            _ => unreachable!(),
        };
    }

    pub fn score(&self, other: &Shape) -> u32 {
        let mut s = self.value();
        s += self.vs(other).value();
        return s;
    }

    fn value(&self) -> u32 {
        return match self {
            Self::Rock => 1,
            Self::Paper => 2,
            Self::Scissors => 3,
        };
    }

    fn vs(&self, other: &Shape) -> Outcome {
        if *self == *other {
            return Outcome::Draw;
        }
        return match self {
            Shape::Rock => {
                match other {
                    Shape::Scissors => Outcome::Win,
                    Shape::Paper => Outcome::Loss,
                    Shape::Rock => unreachable!(),
                }
            },
            Shape::Paper => {
                match other {
                    Shape::Rock => Outcome::Win,
                    Shape::Scissors => Outcome::Loss,
                    Shape::Paper => unreachable!(),
                }
            },
            Shape::Scissors => {
                match other {
                    Shape::Rock => Outcome::Loss,
                    Shape::Paper => Outcome::Win,
                    Shape::Scissors => unreachable!(),
                }
            },
        };
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_rock_outcomes() {
        let play = Shape::Rock;
        assert_eq!(play.vs(Shape::Rock), Outcome::Draw);
        assert_eq!(play.vs(Shape::Paper), Outcome::Loss);
        assert_eq!(play.vs(Shape::Scissors), Outcome::Win);
    }

    #[test]
    fn test_paper_outcomes() {
        let play = Shape::Paper;
        assert_eq!(play.vs(Shape::Rock), Outcome::Win);
        assert_eq!(play.vs(Shape::Paper), Outcome::Draw);
        assert_eq!(play.vs(Shape::Scissors), Outcome::Loss);
    }

    #[test]
    fn test_scissor_outcomes() {
        let play = Shape::Scissors;
        assert_eq!(play.vs(Shape::Rock), Outcome::Loss);
        assert_eq!(play.vs(Shape::Paper), Outcome::Win);
        assert_eq!(play.vs(Shape::Scissors), Outcome::Draw);
    }

    #[test]
    fn test_score() {
        assert_eq!(Shape::Paper.score(Shape::Rock), 8);
        assert_eq!(Shape::Rock.score(Shape::Paper), 1);
        assert_eq!(Shape::Scissors.score(Shape::Scissors), 6);
    }

    #[test]
    fn test_finding_shapes_for_outcome() {
        assert_eq!(Shape::Rock.find_shape_to_achieve_outcome(&Outcome::Draw), Shape::Rock);
        assert_eq!(Shape::Paper.find_shape_to_achieve_outcome(&Outcome::Loss), Shape::Rock);
        assert_eq!(Shape::Paper.find_shape_to_achieve_outcome(&Outcome::Win), Shape::Scissors);
        assert_eq!(Shape::Scissors.find_shape_to_achieve_outcome(&Outcome::Loss), Shape::Paper);
        assert_eq!(Shape::Scissors.find_shape_to_achieve_outcome(&Outcome::Win), Shape::Rock);
    }
}
