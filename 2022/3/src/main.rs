use common;

fn main() {

    let input_file = common::parse_args_input_file(&mut std::env::args());
    let contents = std::fs::read_to_string(input_file).expect("Couldn't read contents of input file");
    let mut score = 0;
    let mut badge_score = 0;
    let mut sacks = std::vec::Vec::<String>::new();
    for line in contents.lines() {
        if line.eq("") {
            continue;
        }
        sacks.push(line.clone().to_string());
        if sacks.len() == 3 {
            let a = get_common(&sacks[0], &sacks[1]).iter().cloned().collect::<String>();
            let b = get_common(&sacks[1], &sacks[2]).iter().cloned().collect::<String>();
            let sack_common = get_common(&a, &b);
            assert_eq!(sack_common.len(), 1);
            badge_score += get_priority(sack_common[0]);
            sacks.clear();
        }
        let (first, second) = line.split_at(line.len() / 2);
        let common = get_common(&first.to_string(), &second.to_string());
        assert_eq!(common.len(), 1);
        score += get_priority(common[0]);
    }
    println!("[PART 1] Score: {}", score);
    println!("[PART 2] Score: {}", badge_score);
}

fn get_common(first: &String, second: &String) -> std::vec::Vec::<char> {
    let mut common = std::vec::Vec::<char>::new();
    for x in first.chars() {
        for y in second.chars() {
            if x == y && !common.contains(&x) {
                common.push(x);
            }
        }
    }
    return common;
}

fn get_priority(value: char) -> u32 {
    let priority = u32::from(value);
    if priority < 97 {
        return priority - 64 + 26;
    }
    return priority - 96;
}
