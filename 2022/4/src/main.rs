use std::str::FromStr;

fn main() {
    let input_file = common::parse_args_input_file(&mut std::env::args());
    let contents = std::fs::read_to_string(input_file).expect("Couldn't read contents of input file");
    let mut contained = 0;
    let mut overlapped = 0;
    for line in contents.lines() {
        if line.eq("") {
            continue;
        }
        let split_line = line.split_once(",").expect("Line didn't contain a pair delimiter");
        let left = split_line.0.split_once("-").expect("Left side didn't contain a range delimiter");
        let right = split_line.1.split_once("-").expect("Right side didn't contain a range delimiter");

        let lmin = u32::from_str(left.0).unwrap();
        let lmax = u32::from_str(left.1).unwrap();
        let rmin = u32::from_str(right.0).unwrap();
        let rmax = u32::from_str(right.1).unwrap();

        if (lmin >= rmin && lmax <= rmax) || (rmin >= lmin && rmax <= lmax) {
            contained += 1;
            println!("{}-{},{}-{}", lmin, lmax, rmin, rmax);
        }

        if (lmin > rmax) || (lmax < rmin) {
            continue;
        }
        overlapped += 1;
    }
    println!("[PART 1] Pairs fully containing another: {}", contained);
    println!("[PART 2] Pairs with any overlap: {}", overlapped);
}
