use std::str::FromStr;

fn main() {
    let input_file = common::parse_args_input_file(&mut std::env::args());
    let contents = std::fs::read_to_string(input_file).expect("Couldn't read contents of input file");
    let mut stacks = get_initial_stacks();
    let mut stacks2 = get_initial_stacks();
    for line in contents.lines() {
        if line.eq("") {
            continue;
        }
        let words: Vec<&str> = line.split(' ').collect();
        assert_eq!(words.len(), 6);
        let mut count = usize::from_str(words[1]).unwrap();
        let mut part2_count = count;
        let from = usize::from_str(words[3]).unwrap() - 1;
        let to = usize::from_str(words[5]).unwrap() - 1;
        // Part 1 - CrateMover 9000
        while count > 0 {
            count -= 1;
            let x = stacks[from].pop();
            if x.is_none() {
                println!("Breaking operation early, source stack empty: {} items from {} to {}", count, from, to);
                break;
            }
            else {
                stacks[to].push(x.unwrap());
            }
        }
        // Part 2 - CrateMover 9001, now with more leather.
        // println!("Moving {} from stack: {:?}", part2_count, stacks2[from]);
        if part2_count > stacks2[from].len() {
            part2_count = stacks2[from].len();
        }
        let split_at = stacks2[from].len() - part2_count;
        let mut clone = stacks2[from].clone();
        let (remainder, moved) = clone.split_at_mut(split_at);
        stacks2[from] = remainder.to_vec();
        for x in moved {
            stacks2[to].push(*x);
        }
        // println!("Source Stack '{}': {:?}", from, stacks2[from]);
        // println!("Destination Stack '{}': {:?}", to, stacks2[to]);
    }
    let mut result = String::with_capacity(stacks.len());
    for stack in stacks {
        result.push(stack[stack.len()-1]);
    }
    let mut result2 = String::with_capacity(stacks2.len());
    for stack in stacks2 {
        result2.push(stack[stack.len()-1]);
    }
    println!("[PART 1] Top bins in stacks are: {}", result);
    println!("[PART 2] Top bins in stacks are: {}", result2);
}

fn get_initial_stacks() -> std::vec::Vec::<std::vec::Vec::<char>> {
    let mut stacks = std::vec::Vec::<std::vec::Vec::<char>>::new();
    stacks.push(vec!['Z', 'J', 'G']);
    stacks.push(vec!['Q', 'L', 'R', 'P', 'W', 'F', 'V', 'C']);
    stacks.push(vec!['F', 'P', 'M', 'C', 'L', 'G', 'R']);
    stacks.push(vec!['L', 'F', 'B', 'W', 'P', 'H', 'M']);
    stacks.push(vec!['G', 'C', 'F', 'S', 'V', 'Q']);
    stacks.push(vec!['W', 'H', 'J', 'Z', 'M', 'Q', 'T', 'L']);
    stacks.push(vec!['H', 'F', 'S', 'B', 'V']);
    stacks.push(vec!['F', 'J', 'Z', 'S']);
    stacks.push(vec!['M', 'C', 'D', 'P', 'F', 'H', 'B', 'T']);
    return stacks;
}
