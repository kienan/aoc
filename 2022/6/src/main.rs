fn main() {
    let contents = std::fs::read_to_string("./input").expect("Failed to read input file");
    let mut x: usize = 0;
    while x < contents.len() - 3 {
        let slice = contents.get(x..x+4).expect("Couldn't slice it");
        if !has_duplicates(slice) {
            println!("[PART 1] First 4 with no duplicates ends at index {}, after the {}th byte", x+3, x+4);
            break;
        }
        x +=1;
    }
    while x < contents.len() - 13 {
        let slice = contents.get(x..x+14).expect("Couldn't slice it");
        if !has_duplicates(slice) {
            println!("[PART 2] First 4 with no duplicates ends at index {}, after the {}th byte", x+13, x+14);
            break;
        }
        x +=1;
    }
}

fn has_duplicates(s: &str) -> bool {
    let mut x: usize = 0;
    while x < s.len() {
        let mut y: usize = x + 1;
        while y < s.len() {
            if x == y {
                y += 1;
                continue;
            }
            if s[x..x+1] == s[y..y+1] {
                return true;
            }
            y += 1;
        }
        x += 1;
    }
    return false;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_has_duplicates() {
        assert_eq!(has_duplicates("asdf"), false);
        assert_eq!(has_duplicates("addf"), true);
        assert_eq!(has_duplicates("asda"), true);
        assert_eq!(has_duplicates("aadf"), true);
        assert_eq!(has_duplicates("aada"), true);
        assert_eq!(has_duplicates("aaaa"), true);
    }
}
