use std::str::FromStr;

fn main() {
    let mut t = Tree::create();
    let mut current_directory: usize = 0;

    let input_file = common::parse_args_input_file(&mut std::env::args());
    let content = std::fs::read_to_string(input_file).expect("Couldn't read input file");
    for line in content.lines() {
        if line.eq("") {
            continue;
        }
        if line.chars().nth(0) == Some('$') {
            // Parse command
            let words: std::vec::Vec<& str> = line.split(' ').collect();
            if words[1].eq("cd") {
                if words[2].eq("..") {
                    let cd = t.nodes[current_directory].as_ref().unwrap();
                    if cd.parent.is_none() {
                        unreachable!();
                    }
                    current_directory = cd.parent.unwrap();
                }
                else if words[2].eq("/") {
                    current_directory = 0;
                }
                else {
                    current_directory = t.find_child_by_name(current_directory, words[2]).unwrap();
                }
                println!("Changed directory to index: {}", current_directory);
            }
        }
        else {
            // In this 'puzzle', the only entries without '$ ' prefix are
            // results from '$ ls'.
            t.add_node_by_parse_line(current_directory, line);
        }
    }

    let mut part1 = 0;
    let mut part2_idx = 0;
    let mut part2_value = t.get_size(0);
    let fs_size = 70000000;
    let used_space = part2_value;
    let threshold = 30000000;
    t.nodes[0].as_ref().unwrap().print(&t, 0);
    for (idx, node) in t.nodes.iter().enumerate() {
        if node.as_ref().unwrap().node_type == NodeType::Directory {
            let size = t.get_size(idx);
            if size <= 100000 {
                part1 += size;
            }
            println!("Size of directory '{}': {}", node.as_ref().unwrap().name, size);

            // Potential space freed by deletion
            let new_used_space_if_deleted = used_space - size;
            let new_free_space = fs_size - new_used_space_if_deleted;
            if new_free_space >= threshold {
                // Deleting this directory could let the update run
                // however, we want the largest "used_space" remaining (so the
                // smallest directory deleted).
                println!("Deleting directory index {} ('{}') would leave {} free", idx, node.as_ref().unwrap().name, new_free_space);
                if size < part2_value {
                    part2_value = size;
                    part2_idx = idx;
                }
            }
        }
    }
    println!("[PART 1] {}", part1);
    println!("[PART 2] Deleting directory index {} size {}", part2_idx, part2_value);
}

#[derive(PartialEq)]
enum NodeType {
    Directory,
    File,
}

struct Node {
    parent: Option<usize>,
    name: String,
    node_type: NodeType,
    size: u32,
    children: std::vec::Vec<usize>,
}

impl Node {
    fn parse(line: &str) -> Node {
        let words: std::vec::Vec<&str> = line.split(' ').collect();
        let mut n = Node {
            parent: None,
            name: words[1].to_string(),
            node_type: NodeType::Directory,
            size: 0,
            children: std::vec::Vec::<usize>::new(),
        };
        if !words[0].eq("dir") {
            n.node_type = NodeType::File;
            n.size = u32::from_str(words[0]).expect("File size is not an integer value");
        }
        return n;
    }

    fn print(&self, tree: &Tree, indent: usize) {
        for _ in 0..indent {
            print!(" ");
        }
        print!("- {} ", self.name);
        if self.node_type == NodeType::Directory {
            println!("(dir)");
        }
        else {
            println!("(file, size={})", self.size);
        }
        for c_id in self.children.iter() {
            tree.nodes[*c_id].as_ref().unwrap().print(tree, indent + 2);
        }
    }

    fn get_size(&self, tree: &Tree) -> u32 {
        if self.node_type == NodeType::File {
            return self.size;
        }
        else {
            let mut size: u32 = 0;
            for c in self.children.iter() {
                size += tree.get_size(*c);
            }
            return size;
        }
    }
}

struct Tree {
    nodes: std::vec::Vec<Option<Node>>,
}

impl Tree {

    fn create() -> Tree {
        let mut t = Tree {
            nodes: std::vec::Vec::<Option<Node>>::new(),
        };
        t.nodes.push(Some(Node {
            parent: None,
            name: "/".to_string(),
            node_type: NodeType::Directory,
            size: 0,
            children: std::vec::Vec::<usize>::new(),
        }));
        return t;
    }

    fn find_by_name(&self, name: &str) -> Option<usize> {
        let mut index = 0;
        for x in &self.nodes {
            if x.is_none() {
                index += 1;
                continue;
            }
            if x.as_ref().unwrap().name.eq(name) {
                return Some(index);
            }
            index += 1;
        }
        return None;
    }

    fn find_child_by_name(&self, start: usize, name: &str) -> Option<usize> {
        for n_id in self.nodes[start].as_ref().unwrap().children.iter() {
            let n = &self.nodes[*n_id];
            if n.as_ref().unwrap().name.eq(name) {
                return Some(*n_id);
            }
        }
        return None;
    }

    fn add_node_by_parse_line(&mut self, parent: usize, line: &str) -> Result<usize, &'static str> {
        if self.nodes.get_mut(parent).expect("Not in storage").is_none() {
            return Result::Err("Parent node not found by id");
        }
        let mut n = Node::parse(line);
        n.parent = Some(parent);
        self.nodes.push(Some(n));
        let new_index = self.nodes.len() - 1;
        let p = self.nodes.get_mut(parent).expect("Not in storage");
        p.as_mut().unwrap().children.push(new_index);
        return Ok(new_index);
    }

    fn add_node_by_parent_string_parse_line(&mut self, parent: &str, line: &str) -> Result<usize, &'static str> {
        let p = self.find_by_name(parent);
        if p.is_none() {
            return Result::Err("Parent node not found by name");
        }
        return self.add_node_by_parse_line(p.unwrap(), line);
    }

    // fn invalidate_directory_size_cache(&mut self, start: usize) {
    //     let mut n = self.nodes.get_mut(start).expect("start node doesn't exist");
    //     while n.is_some() {
    //         if n.as_ref().unwrap().node_type == NodeType::Directory {
    //             n.as_mut().unwrap().dir_size_cache_dirty = true;
    //         }
    //         if n.as_ref().unwrap().parent.is_none() {
    //             break;
    //         }
    //         let p = n.as_ref().unwrap().parent.unwrap();
    //         n = self.nodes.get_mut(p).expect("parent node doesn't exist");
    //     }
    // }

    fn get_size(&self, start: usize) -> u32 {
        return self.nodes[start].as_ref().unwrap().get_size(self);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_by_name() {
        let mut t = Tree::create();
        t.nodes.push(Some(Node::parse("dir a")));
        t.nodes.push(Some(Node::parse("14848514 b.txt")));
        t.nodes.push(Some(Node::parse("8504156 c.dat")));
        assert_eq!(t.find_by_name("b.txt"), Some(2));
        assert_eq!(t.find_by_name("c.txt"), None);
    }

    #[test]
    fn test_add_by_parsing() {
        let mut t = Tree::create();
        let a_idx = t.add_node_by_parent_string_parse_line("/", "dir a").unwrap();
        assert!(t.nodes[0].as_ref().unwrap().children.contains(&a_idx));
        let b_idx = t.add_node_by_parent_string_parse_line("/", "14848514 b.txt").unwrap();
        assert!(t.nodes[0].as_ref().unwrap().children.contains(&b_idx));
        let c_idx = t.add_node_by_parent_string_parse_line("a", "8504156 c.dat").unwrap();
        assert!(t.nodes[a_idx].as_ref().unwrap().children.contains(&c_idx));

        let x = t.find_by_name("c.dat");
        assert_eq!(x, Some(c_idx));
        let c = t.nodes.get(x.unwrap()).unwrap();
        assert_eq!(c.as_ref().unwrap().parent, Some(a_idx));
    }
}
