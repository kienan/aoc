fn main() {
    println!("Hello, world!");
    let input_file = common::parse_args_input_file(&mut std::env::args());
    let contents = std::fs::read_to_string(input_file).expect("Failed to read input file");
    let mut map = Map::from_string(&contents);
    map.calculate_visibility();
    let mut visible = 0;
    for p in map.data.iter() {
        if p.visible.is_some() && p.visible.unwrap() {
            visible += 1;
        }
    }
    println!("[PART 1] {} visible trees", visible);

    map.calculate_scores();
    let mut high_score = 0;
    let mut high_score_index = 0;
    for (index, point) in map.data.iter().enumerate() {
        assert!(point.score.is_some());
        if point.score.unwrap() > high_score {
            high_score = point.score.unwrap();
            high_score_index = index;
        }
    }
    println!("[PART 2] Highest scenic score at index {}: {}", high_score_index, high_score);
}

struct Point {
    height: u32,
    visible: Option<bool>,
    score: Option<u32>,
}

struct Map {
    width: u32,
    height: u32,
    data: std::vec::Vec<Point>,
}

impl Map {
    fn from_string(s: &String) -> Map {
        let mut h: u32 = 0;
        let mut map = Map {
            width: 0,
            height: 0,
            data: std::vec::Vec::<Point>::new(),
        };
        for line in s.lines() {
            if line.eq("") {
                continue;
            }
            if map.width == 0 {
                map.width = u32::try_from(line.len()).unwrap();
            }
            else {
                assert_eq!(map.width, u32::try_from(line.len()).unwrap());
            }
            h += 1;
            for c in line.chars() {
                map.data.push(Point {
                    // this converts the character code to u32, so assume it's 0-9
                    // and substract the ascii code index
                    height: u32::from(c) - 48,
                    visible: None,
                    score: None,
                });
            }
        }
        map.height = h;
        return map;
    }

    fn point_index(&self, x: u32, y: u32) -> usize {
        return Map::point_index_s(x, y, self.width, self.height);
    }

    fn point_index_s(x: u32, y: u32, width: u32, height: u32) -> usize {
        assert!(x < width);
        assert!(y < height);
        return usize::try_from(y * width + x).unwrap();
    }

    fn index_to_point(&self, index: usize) -> (u32, u32) {
        return Map::index_to_point_s(index, self.height);
    }

    fn index_to_point_s(index: usize, height: u32) -> (u32, u32) {
        let x = u32::try_from(index).unwrap() % height;
        let y = u32::try_from(index).unwrap() / height;
        return (x, y);
    }

    fn is_edge_point(&self, x: u32, y: u32) -> bool {
        return (x == 0 || x == (self.width - 1)) || (y == 0 || y == (self.height - 1));
    }

    fn calculate_visibility(&mut self) {
        // Mark all edges as visible
        let mut x = 0;
        let mut y = 0;
        while x < self.width {
            let p1 = self.point_index(x, 0);
            let p2 = self.point_index(x, self.height - 1);
            self.data[p1].visible = Some(true);
            self.data[p2].visible = Some(true);
            x += 1;
        }
        while y < self.height {
            let p1 = self.point_index(0, y);
            let p2 = self.point_index(self.width - 1, y);
            self.data[p1].visible = Some(true);
            self.data[p2].visible = Some(true);
            y += 1;
        }

        let mut index = 0;
        while index < self.data.len() {
            if self.data[index].visible.is_some() {
                index += 1;
                continue;
            }
            let (x, y) = Map::index_to_point_s(index, self.height);
            let mut cur_x = x;
            let mut cur_y = y;
            // Look "up", negative y
            let mut vis_n = true;
            let mut vis_s = true;
            let mut vis_e = true;
            let mut vis_w = true;
            let mut last_height = self.data[index].height;
            while cur_y > 0 {
                cur_y -= 1;
                let p = Map::point_index_s(cur_x, cur_y, self.width, self.height);
                println!("[{}] Testing N point ({}, {})", index, cur_x, cur_y);
                if self.data[p].height >= last_height {
                    vis_n = false;
                    break;
                }
                last_height = std::cmp::max(last_height, self.data[p].height);
            }
            cur_y = y;
            last_height = self.data[index].height;
            while cur_y < self.height - 1 {
                cur_y += 1;
                let p = Map::point_index_s(cur_x, cur_y, self.width, self.height);
                println!("[{}] Testing S point ({}, {})", index, cur_x, cur_y);
                if self.data[p].height >= last_height {
                    vis_s = false;
                    break;
                }
                last_height = std::cmp::max(last_height, self.data[p].height);
            }
            cur_y = y;
            last_height = self.data[index].height;
            while cur_x > 0 {
                cur_x -= 1;
                let p = Map::point_index_s(cur_x, cur_y, self.width, self.height);
                println!("[{}] Testing W point ({}, {})", index, cur_x, cur_y);
                if self.data[p].height >= last_height {
                    vis_w = false;
                    break;
                }
                last_height = std::cmp::max(last_height, self.data[p].height);
            }
            cur_x = x;
            last_height = self.data[index].height;
            while cur_x < self.width -1  {
                cur_x += 1;
                let p = Map::point_index_s(cur_x, cur_y, self.width, self.height);
                println!("[{}] Testing E point ({}, {})", index, cur_x, cur_y);
                if self.data[p].height >= last_height {
                    vis_e = false;
                    break;
                }
                last_height = std::cmp::max(last_height, self.data[p].height);
            }
            if vis_n || vis_s || vis_e || vis_w {
                self.data[index].visible = Some(true);
            }
            else {
                self.data[index].visible = Some(false);
            }
            index += 1;
        }
    }

    fn calculate_scores(&mut self) {
        let mut index = 0;
        while index < self.data.len() {
            if self.data[index].score.is_some() {
                index += 1;
                continue;
            }
            let (x, y) = Map::index_to_point_s(index, self.height);
            let mut cur_x = x;
            let mut cur_y = y;
            // Look "up", negative y
            let mut vis_n = 0;
            let mut vis_s = 0;
            let mut vis_e = 0;
            let mut vis_w = 0;
            let mut last_height = self.data[index].height;
            while cur_y > 0 {
                cur_y -= 1;
                let p = Map::point_index_s(cur_x, cur_y, self.width, self.height);
                println!("[{}] Testing N point ({}, {})", index, cur_x, cur_y);
                if self.data[p].height >= last_height {
                    vis_n += 1;
                    break;
                }
                last_height = std::cmp::max(last_height, self.data[p].height);
                vis_n += 1;
            }
            cur_y = y;
            last_height = self.data[index].height;
            while cur_y < self.height - 1 {
                cur_y += 1;
                let p = Map::point_index_s(cur_x, cur_y, self.width, self.height);
                println!("[{}] Testing S point ({}, {})", index, cur_x, cur_y);
                if self.data[p].height >= last_height {
                    vis_s += 1;
                    break;
                }
                vis_s += 1;
                last_height = std::cmp::max(last_height, self.data[p].height);
            }
            cur_y = y;
            last_height = self.data[index].height;
            while cur_x > 0 {
                cur_x -= 1;
                let p = Map::point_index_s(cur_x, cur_y, self.width, self.height);
                println!("[{}] Testing W point ({}, {})", index, cur_x, cur_y);
                if self.data[p].height >= last_height {
                    vis_w += 1;
                    break;
                }
                vis_w += 1;
                last_height = std::cmp::max(last_height, self.data[p].height);
            }
            cur_x = x;
            last_height = self.data[index].height;
            while cur_x < self.width -1  {
                cur_x += 1;
                let p = Map::point_index_s(cur_x, cur_y, self.width, self.height);
                println!("[{}] Testing E point ({}, {})", index, cur_x, cur_y);
                if self.data[p].height >= last_height {
                    vis_e += 1;
                    break;
                }
                vis_e += 1;
                last_height = std::cmp::max(last_height, self.data[p].height);
            }
            println!("[{}] {}N, {}S, {}E, {}W: score {}", index, vis_n, vis_s, vis_e, vis_w,
                     vis_n * vis_s * vis_e * vis_w);
            self.data[index].score = Some(vis_n * vis_s * vis_e * vis_w);
            index += 1;
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn point_index() {
        let map = Map {
            height: 5,
            width: 5,
            data: std::vec::Vec::<Point>::new(),
        };
        assert_eq!(map.point_index(0, 4), 20);
        assert_eq!(map.point_index(4, 0), 4);
        assert_eq!(map.point_index(2, 2), 12);
        assert_eq!(map.index_to_point(20), (0, 4));

        assert_eq!(map.is_edge_point(2, 0), true);
        assert_eq!(map.is_edge_point(2, 2), false);
        assert_eq!(map.is_edge_point(2, 4), true);
        assert_eq!(map.is_edge_point(0, 2), true);
        assert_eq!(map.is_edge_point(4, 2), true);
        assert_eq!(map.is_edge_point(1, 1), false);
    }

    #[test]
    fn example() {
        let mut map = Map::from_string(&std::fs::read_to_string("test_input").expect("Failed to read test_input file"));
        assert_eq!(map.width, 5);
        assert_eq!(map.height, 5);
        assert_eq!(map.data[0].height, 3);

        let mut invisible_points = std::vec::Vec::<usize>::new();
        let mut visible_count = 0;
        map.calculate_visibility();
        for (index, point) in map.data.iter().enumerate() {
            assert!(point.visible.is_some());
            if point.visible.unwrap() {
                visible_count += 1;
            }
            else {
                invisible_points.push(index);
            }
        }
        assert_eq!(visible_count, 21);
        assert_eq!(invisible_points.len(), 4);
        assert_eq!(invisible_points, vec![8, 12, 16, 18]);

        map.calculate_scores();
        assert_eq!(map.data[7].score.unwrap(), 4);
        assert_eq!(map.data[17].score.unwrap(), 8);
        let mut high_score = 0;
        let mut high_score_index = 0;
        for (index, point) in map.data.iter().enumerate() {
            assert!(point.score.is_some());
            if point.score.unwrap() > high_score {
                high_score = point.score.unwrap();
                high_score_index = index;
            }
        }
        assert_eq!(high_score, 8);
        assert_eq!(high_score_index, 17);
    }
}
