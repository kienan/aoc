pub mod tree;

pub fn parse_args_input_file(args: &mut std::env::Args) -> String {
    let mut input_file = Some("input".to_string());
    while args.len() > 0 {
        let arg = args.nth(0);
        if arg.unwrap().eq("-f") {
            let value = args.nth(0);
            if value.is_some() {
                input_file = value;
            }
            else {
                println!("No value after argument: '-f'");
            }
        }
    }
    return input_file.unwrap();
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Point {
    pub x: i64,
    pub y: i64,
}

impl Point {
    pub fn displacement(direction: char, magnitude: i64) -> Point {
        let delta = match direction {
            'U' => Point { x: 0, y: magnitude },
            'D' => Point { x: 0, y: -magnitude},
            'L' => Point { x: -magnitude, y: 0 },
            'R' => Point { x: magnitude, y: 0 },
            _ => unreachable!(),
        };
        return delta;
    }

    pub fn add(&mut self, delta: &Point) {
        self.x += delta.x;
        self.y += delta.y;
    }

    pub fn touching(&self, other: &Point) -> bool {
        let d = self.delta(other);
        return d.x.abs() < 2 && d.y.abs() < 2;
    }

    pub fn delta(&self, other: &Point) -> Point {
        return Point {
            x: (self.x - other.x),
            y: (self.y - other.y),
        };
    }

    pub fn towards(&self, other: &Point) -> Point {
        if self.touching(other) {
            return Point {x: 0, y: 0};
        }
        let mut delta = self.delta(other);
        delta.unit();
        let dir_x = if self.x > other.x { -1 } else { 1 };
        delta.x *= dir_x;
        let dir_y = if self.y > other.y { -1 } else { 1 };
        delta.y *= dir_y;
        return delta;
    }

    pub fn step_towards(&self, other: &Point) -> Point {
        let mut delta = Point { x: (self.x - other.x).abs(), y: (self.y - other.y).abs() };
        let dir_x = if self.x > other.x { -1 } else { 1 };
        delta.x *= dir_x;
        let dir_y = if self.y > other.y { -1 } else { 1 };
        delta.y *= dir_y;
        // So it's always only one step
        if delta.x != 0 {
            delta.x /= delta.x.abs();
        }
        if delta.y != 0 {
            delta.y /= delta.y.abs();
        }
        return delta;
    }

    pub fn unit(&mut self) {
        let larger = std::cmp::max(self.x.abs(), self.y.abs()) as f32;
        assert!(larger != 0.0);
        self.x = (self.x.abs() as f32 / larger).ceil() as i64;
        self.y = (self.y.abs() as f32 / larger).ceil() as i64;
    }

    pub fn manhattan_distance(&self, other: &Point) -> u64 {
        let x = (self.x - other.x).abs() as u64;
        let y = (self.y - other.y).abs() as u64;
        return x + y;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn point_towards() {
        let no_movement = Point { x: 0, y: 0 };
        let down = Point { x: 0, y: -1 };
        let up = Point { x: 0, y: 1 };
        let left = Point { x: -1, y: 0 };
        let right = Point { x: 1, y: 0 };
        let diag_dr = Point { x: 1, y: -1 };
        let diag_dl = Point { x: -1, y: -1 };
        let diag_ur = Point { x: 1, y: 1 };
        let diag_ul = Point { x: -1, y: 1 };

        let h = Point { x: 0, y: 0 };

        for x in -1..2 as i64 {
            for y in -1..2 as i64 {
                let p = Point { x: x, y: y };
                println!("{},{}", x, y);
                assert_eq!(p.towards(&h), no_movement);
            }
        }

        let tests = vec![
            vec![Point { x: -2, y: 2 }, diag_dr.clone()],
            vec![Point { x: -1, y: 2 }, diag_dr.clone()],
            vec![Point { x: 0, y: 2 }, down.clone()],
            vec![Point { x: 1, y: 2 }, diag_dl.clone()],
            vec![Point { x: 2, y: 2 }, diag_dl.clone()],
            vec![Point { x: -2, y: -2 }, diag_ur.clone()],
            vec![Point { x: -1, y: -2 }, diag_ur.clone()],
            vec![Point { x: 0, y: -2 }, up.clone()],
            vec![Point { x: 1, y: -2 }, diag_ul.clone()],
            vec![Point { x: 2, y: -2 }, diag_ul.clone()],
            vec![Point { x: -2, y: 1 }, diag_dr.clone()],
            vec![Point { x: -2, y: 0 }, right.clone()],
            vec![Point { x: -2, y: -1 }, diag_ur.clone()],
            vec![Point { x: 2, y: 1 }, diag_dl.clone()],
            vec![Point { x: 2, y: 0 }, left.clone()],
            vec![Point { x: 2, y: -1 }, diag_ul.clone()],
        ];
        for t in tests.iter() {
            println!("{:?}", t[0]);
            assert_eq!(t[0].towards(&h), t[1]);
        }
    }

}
