pub struct Node<T> {
    pub parent: Option<usize>,
    pub data: T,
    pub children: std::vec::Vec<usize>,
}

impl<T> Node<T> {

}

pub struct Tree<T> {
    pub nodes: std::vec::Vec<Node<T>>,
}

impl<T> Tree<T> {
    pub fn new() -> Tree<T> {
        return Self {
            nodes: std::vec::Vec::<Node<T>>::new(),
        };
    }


    pub fn get(&self, index: usize) -> Option<&Node<T>> {
        return self.nodes.get(index);
    }

    pub fn insert(&mut self, data: T, parent: Option<usize>) {
        let node = Node::<T> {
            data: data,
            parent: parent,
            children: std::vec::Vec::<usize>::new(),
        };
        let new_id = self.nodes.len();
        self.nodes.push(node);
        if parent.is_some() {
            match self.nodes.get_mut(parent.unwrap()) {
                Some(v) => {
                    v.children.push(new_id);
                },
                None => {
                    assert!(false, "A parent was given, but not found in the list of nodes");
                }
            };
        }
    }

    // Implementation of A*
    // @see https://en.wikipedia.org/wiki/A*_search_algorithm
    pub fn find_path(&self, start: usize, end: usize) -> Option<std::vec::Vec<usize>> {
        let mut open_set = std::collections::VecDeque::<usize>::new();
        open_set.push_back(start);

        let mut came_from = std::collections::HashMap::<usize, usize>::new();

        // Cheapest path from start to item n currently known
        let mut scores = std::collections::HashMap::<usize, u32>::new();
        scores.insert(start, 0);

        // guesses of cost of path from start to end via n
        let mut estimates = std::collections::HashMap::<usize, u32>::new();

        while open_set.len() > 0 {
            // Get the node in the openSet with the lowest fScore to test
            let current = path_get_next_node_to_test(&open_set, &estimates);
            if current == end {
                return Some(path_from_previous_nodes(&came_from, current));
            }
            open_set.retain(|&x| x != current);
            for neighbour in &self.nodes[current].children {
                assert!(scores.contains_key(&current));
                let score = scores.get(&current).unwrap() + 1; // 1 is normally the weight of the edge from current to neighbour
                let neighbour_score = match scores.get(&neighbour) {
                    None => { u32::MAX },
                    Some(v) => { *v },
                };
                if score < neighbour_score {
                    came_from.insert(*neighbour, current);
                    scores.insert(*neighbour, score);
                    // @TODO
                    // guess cost to reach end via this node: h(n), where h(n) is
                    // a heuristic function; however, I don't have an idea of what
                    // those heuristics may be at this time
                    estimates.insert(*neighbour, score + 10);
                    if !open_set.contains(&neighbour) {
                        open_set.push_back(*neighbour);
                    }
                }
            }
        }
        return None;
    }
}

fn path_from_previous_nodes(from: &std::collections::HashMap<usize, usize>, end: usize) -> std::vec::Vec<usize> {
    let mut path = std::vec::Vec::<usize>::new();
    path.push(end);
    let mut current = from.get(&end);
    while current.is_some() {
        path.insert(0, *current.unwrap());
        current = from.get(&*current.unwrap());
    }
    return path;
}

fn path_get_next_node_to_test(open: &std::collections::VecDeque<usize>, estimates: &std::collections::HashMap<usize, u32>) -> usize {
    let mut current: Option<usize> = None;
    let mut current_value = u32::MAX;
    for n in open.iter() {
        let score = match estimates.get(n) {
            None => {
                u32::MAX
            },
            Some(v) => {
                *v
            },
        };
        if current.is_none() || score < current_value {
            current = Some(*n);
            current_value = score;
        }
    }
    return current.unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new() {
        _ = Tree::<u32>::new();
    }
}
